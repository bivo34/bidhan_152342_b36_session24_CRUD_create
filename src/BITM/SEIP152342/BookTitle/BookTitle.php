<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends  DB{

    public $id;
    public $book_title;
    public $author_name;
    public function __construct()
         {
             parent::__construct(); // TODO: Change the autogenerated stub
          }

    public function set_data($postVariableData=NUll){

        if (array_key_exists("id",$postVariableData)){
            $this->id=$postVariableData['id'];
           }

        if (array_key_exists("book_title",$postVariableData)) {
            $this->book_title = $postVariableData['book_title'];
            }
        if (array_key_exists("author_name",$postVariableData)) {
            $this->author_name = $postVariableData['author_name'];
            }

      }//end of class
public function store(){
          $arrData=array($this->book_title,$this->author_name);
         $sql="insert into book_title(book_title,author_name)VALUES(?,?)";
          $SHT=$this->DBH->prepare($sql);
          $result= $SHT->execute($arrData);
          if($result)
          Message::Message("Data has been inserted successfully");
          else
          Message::Message("Data has not been inserted successfully");
   // header('Location:'.$data);

Utility::redirect('create.php');


    }

}
